#!/bin/bash

if [[ $# -eq 0 ]]; then
    if [ -z $USER ]; then
        exec /bin/bash
    else
    	cd /home/$USER
        exec su $USER
    fi
else
    if [ -z $USER ]; then
        exec $*
    else
    	cd /home/$USER
        exec su -c "$*" $USER
    fi
fi
